import AppKit
import weakref
from mojo.roboFont import OpenWindow
from simpleKerning import *
from simpleKerning.pairListBuilder import PairList
from mojo.events import addObserver, removeObserver
from vanilla import *
from mojo.UI import MultiLineView, AskYesNoCancel, GlyphSequenceEditText
from mojo.extensions import getExtensionDefault, setExtensionDefault
from mojo.roboFont import CurrentFont, AllFonts

class SKMainWindow(object):
	__name__ = KERN_WINDOW_KEY
	MODIFIER_TO_INCREMENT = {
	SHFIT_CODE: 50,
	SHIFT_CMD_CODE: 100,
	ALT_CODE: 1,
	}

	def __init__(self, font):
		self.font = font
		self._keyMonitor = None
		self._pairlist = PairList()
		self._kerningViewGlyphSequence = []
		self._referenceGlyphNames = []
		self._blockRecursion = False  # for pairlistNameComboBox
		self.RTL = True
		self._beforeContextGlyphNames = []  # glyph name sequence
		self._afterContextGlyphNames = []  # glyph name sequence
		self._beforeContextText = ''
		self._afterContextText = ''
		self._currentPair = None
		self._currentPairIndex = 0
		self._pairNumbers = 0
		self._mutliLineDisplayStates = MV_OPTIONS
		self._pairListDict = {}
		self._pairListsInstructionsDict = getExtensionDefault(EXTENSION_PAIRLIST_KEY, {})
		self._allFonts = {}

		self.w = Window((800, 800), minSize=(300, 300))
		self.w.kerningSequenceView = MultiLineView((0, 70, -200, -35),
										pointSize=50)
		self.w.kerningSequenceView._glyphLineView._buffer = 70
		self.w.kerningSequenceView.setDisplayStates(self._mutliLineDisplayStates)
		self.w.kerningSequenceView.setLineHeight(0)
		self._pointSize = self.w.kerningSequenceView.getPointSize()
		self._lineHeight = self.w.kerningSequenceView.getLineHeight()
		self._newLineGlyph = self.w.kerningSequenceView.createNewLineGlyph()

		self.w.pairListView = List((-200, 40, -0, -0), columnDescriptions=[{"title": FIRST_COLUMN}, {"title": SECOND_COLUMN}],
							allowsMultipleSelection=True, allowsSorting=False,
							selectionCallback=self._selectPairInListCallback, items=self._pairlist)
		self.w.leftEntryComboBox = ComboBox((10, 0, 200, 60), [], callback=self._leftEntryComboBoxCallback, continuous=False)
		self.w.rightEntryComboBox = ComboBox((430, 0, 200, 60), [], callback=self._rightEntryComboBoxCallback, continuous=False)
		self.w.pairlistNameComboBox = ComboBox((-200, 11, -10, 20), self._pairListsInstructionsDict.keys(), callback=self._pairlistNameComboBoxCallback, continuous=False)

		self.w.kernValueInput = EditText((220, 10, 200, 25), callback=self._kernValueInputCallback, continuous=False)
		self.w.leftContextInput = GlyphSequenceEditText((10, 0, 400, 50), font=font, callback=self._leftContextInputCallback)
		self.w.rightContextInput = GlyphSequenceEditText((420, 0, 200, 50), font=font, callback=self._rightContextInputCallback)
		self.w.referenceTextInput = GlyphSequenceEditText((10, 40, 400, 25), font=font, callback=self._referenceTextInputCallback)
		self.w.kernValueInput._nsObject.setAlignment_(AppKit.NSTextAlignmentCenter)
		self.w.referenceTextInput._nsObject.setAlignment_(AppKit.NSTextAlignmentCenter)
		self.w.leftContextInput._nsObject.setAlignment_(AppKit.NSTextAlignmentRight)
		addObserver(self, "_fontDidOpen", "fontDidOpen")
		addObserver(self, "_fontWillClose", "fontWillClose")
		for f in AllFonts():
			self._addFont(f)
		self.setFont(font)
		self.w.bind("close", self._closeWindow)
		self.w.bind("resize", self._windowSizeChanged)
		self.w.open()
		self.w.vanillaWrapper = weakref.ref(self)
		self._windowSizeChanged(None)
		self._startOverridingRFkeys()

	def _glyphNamesToGlyphs(self, glyphNameSequence):
		return [self._font[g] for g in glyphNameSequence]

	def _currentPairSet(self, pair):
		if isKerningPairValid(pair, self._font.keys(), self._font.groups.keys()):
			self._currentPair = pair
			flatPairs = flattenPair(self.currentPair, self._font.groups)
			self._currentFlatPair = flatPairs[0]
			if self._isCurrentPairRTL():
				self.RTL = True
			else:
				self.RTL = False
			self._mutliLineDisplayStates['Right to Left'] = self.RTL
			self.w.kerningSequenceView.setDisplayStates(self._mutliLineDisplayStates)
			self._updateWindowEntries()
			self._processGlyphSequence()

	def _isCurrentPairRTL(self):
		for g in self._currentFlatPair:
			if self._font[g].lib.get(RTL_KEY, self.RTLMap[g]) == 1:
				return True
		return False

	def _currentPairGet(self):
		return self._currentPair

	currentPair = property(_currentPairGet, _currentPairSet)

	def _fontDidOpen(self, info):
		self._addFont(info["font"])

	def _addFont(self, font):
		font.naked().identifier = hash(font)
		self._allFonts[font.naked().identifier] = font

	def _fontWillClose(self, info):
		del self._allFonts[info["font"].naked().identifier]

	def setFont(self, font):
		if font is not None:
			defconFont = font.naked()
			self._font = font
			self.w.kerningSequenceView.setFont(font)
			self.RTLMap = font.rtlMap
			self._cmap = font.naked().unicodeData
			self._pairlist.setFont(self._font)
			self.w.leftContextInput.setFont(defconFont)
			self.w.rightContextInput.setFont(defconFont)
			self.w.referenceTextInput.setFont(defconFont)
			self._glyphToGroupMap = getGlyphToGroupDictionary(self._font.groups)

	def _windowSizeChanged(self, sender):
		left, top, width, height = self.w.getPosSize()
		topEntryWidth = (width - 240)/3
		self.w.leftEntryComboBox.setPosSize((10, 11, topEntryWidth, 20))
		self.w.kernValueInput.setPosSize((topEntryWidth+20, 10, topEntryWidth, 20))
		self.w.rightEntryComboBox.setPosSize((topEntryWidth*2+30, 11, topEntryWidth, 20))
		self.w.referenceTextInput.setPosSize((10, 40, (width - 220), 25))
		bottomEntryWidth = (width - 230)/2
		self.w.leftContextInput.setPosSize((10, -30, bottomEntryWidth, 25))
		self.w.rightContextInput.setPosSize((bottomEntryWidth+25, -30, bottomEntryWidth, 25))

	def _leftContextInputCallback(self, sender):
		glyphSequence = sender.get()
		txt = sender.getRaw()
		if self.RTL:
			self._afterContextGlyphNames = glyphSequence
			self._afterContextText = txt
		else:
			self._beforeContextGlyphNames = glyphSequence
			self._beforeContextText = txt
		self._processGlyphSequence()

	def _rightContextInputCallback(self, sender):
		glyphSequence = sender.get()
		txt = sender.getRaw()
		if not self.RTL:
			self._afterContextGlyphNames = glyphSequence
			self._afterContextText = txt
		else:
			self._beforeContextGlyphNames = glyphSequence
			self._beforeContextText = txt
		self._processGlyphSequence()

	def _referenceTextInputCallback(self, sender):
		glyphSequence = sender.get()
		self._referenceGlyphNames = glyphSequence
		self._processGlyphSequence()

	def _setAfterContext(self, txt):
		if self.RTL:
			inputObj = self.w.leftContextInput
		else:
			inputObj = self.w.rightContextInput
		inputObj.set(txt)
		self._afterContextGlyphNames = inputObj.get()
		self._afterContextText = txt

	def _setBeforeContext(self, txt):
		if not self.RTL:
			inputObj = self.w.leftContextInput
		else:
			inputObj = self.w.rightContextInput
		inputObj.set(txt)
		self._beforeContextGlyphNames = inputObj.get()
		self._beforeContextText = txt

	def _setReferenceText(self, txt):
		self.w.referenceTextInput.set(txt)
		glyphSequence = self.w.referenceTextInput.get()
		self._referenceGlyphNames = glyphSequence

	def _getAfterContext(self):
		if self.RTL:
			self.w.leftContextInput.getRaw()
		else:
			self.w.rightContextInput.getRaw()

	def _getBeforeContext(self):
		if not self.RTL:
			self.w.leftContextInput.getRaw()
		else:
			self.w.rightContextInput.getRaw()

	def _getReferenceText(self):
		return self.w.referenceTextInput.getRaw()

	referenceText = property(_getReferenceText, _setReferenceText)
	beforeContext = property(_getBeforeContext, _setBeforeContext)
	afterContext = property(_getAfterContext, _setAfterContext)

	def _kernValueInputCallback(self, sender):
		inText = sender.get()
		selectedPairsIndices = self.w.pairListView.getSelection()
		selectedPairs = [self._pairlist[i] for i in selectedPairsIndices]
		if inText == '':
			answer = AskYesNoCancel("Remove kerning for these pairs?", informativeText="'No' will only remove the pairs from the current list.", default=1)
			if answer >= 0:
				self._removeSelectedPairs(answer)
			else:
				self.w.kernValueInput.set(self._font.kerning.get(self.currentPair, 0))
			return
		try:
			value = int(sender.get())
		except ValueError:
			print("Please enter an integer value!")
			return
		if len(selectedPairs) > 1:
			for pair in selectedPairs:
				self._font.kerning[pair] = value
			return
		self._font.kerning[self.currentPair] = value
		self._addCurrentPairToPairList()
		self._processGlyphSequence()

	def _leftEntryComboBoxCallback(self, sender):
		entry = sender.get()
		if entry:
			self._setLeftEntry(entry)

	def _setLeftEntry(self, entry):
		first, second = self.currentPair
		if self.RTL:
			second = entry
		else:
			first = entry
		self.currentPair = (first, second)

	def _rightEntryComboBoxCallback(self, sender):
		entry = sender.get()
		if entry:
			self._setRightEntry(entry)

	def _setRightEntry(self, entry):
		first, second = self.currentPair
		if self.RTL:
			first = entry
		else:
			second = entry
		self.currentPair = (first, second)

	def _pairlistNameComboBoxCallback(self, sender):
		if self._blockRecursion:
			# Message("Recursion blocked y'all!")
			self._blockRecursion = False
			return
		inputName = sender.get()
		if inputName is None:
			inputName = self._pairlist.name
		if self._pairlist.name != inputName:
			if inputName == "":
				# delete
				answer = AskYesNoCancel("Delete the pair list '%s'?" %self._pairlist.name, default=1)
				if answer == 1:
					self._pairListsInstructionsDict.pop(self._pairlist.name, None)
					self._pairListDict.pop(self._pairlist.name, None)
					self._pairlist.name = ""
				self._updatePairlistNameComboBox()
			elif inputName not in self._pairListsInstructionsDict:
				# save/rename
				if self._pairlist.name != "":
					self._pairListsInstructionsDict.pop(self._pairlist.name, None)
					self._pairListDict.pop(self._pairlist.name, None)
				self._pairlist.name = inputName
				self._pairListsInstructionsDict[self._pairlist.name] = self._pairlist.getInstructions()
				self._pairListDict[self._pairlist.name] = self._pairlist
				self._updatePairlistNameComboBox()
			else:
				# load
				try:
					pairlist = self._pairListDict[inputName]
				except KeyError:
					pairlist = PairList(self._font)
					# print(self._pairListsInstructionsDict[inputName].keys())
					pairlist.loadInstructions(self._pairListsInstructionsDict[inputName])
				pairlist.name = inputName
				self.setPairList(pairlist)
				self._updatePairListView()
				self._processGlyphSequence()
		self._blockRecursion = False

	def _updatePairlistNameComboBox(self):
		self._pairListsInstructionsDict.pop("", None)
		items = sorted(self._pairListsInstructionsDict.keys())
		if items != sorted(self.w.pairlistNameComboBox.getItems()):
			self.w.pairlistNameComboBox.setItems(items)
			self._blockRecursion = True
		if self._pairlist.name != self.w.pairlistNameComboBox.get():
			if self._pairlist.name == "":
				self.w.pairlistNameComboBox.set("")
			else:
				self.w.pairlistNameComboBox._nsObject.selectItemWithObjectValue_(self._pairlist.name)
			self._blockRecursion = True

	def _closeWindow(self, sender):
		for pairlistName, pairlist in self._pairListDict.items():
			self._pairListsInstructionsDict[pairlistName] = pairlist.getInstructions()
		setExtensionDefault(EXTENSION_PAIRLIST_KEY, self._pairListsInstructionsDict)
		self._stopOverridingRFkeys()
		removeObserver(self, "fontDidOpen")
		removeObserver(self, "fontWillClose")

	def _start(self, sender):
		self._startOverridingRFkeys()

	def _startOverridingRFkeys(self):
		self._stopOverridingRFkeys()
		self._keyMonitor = AppKit.NSEvent.addLocalMonitorForEventsMatchingMask_handler_(
			AppKit.NSKeyDownMask, self._keyDown)

	def _stopOverridingRFkeys(self):
		if self._keyMonitor is not None:
			AppKit.NSEvent.removeMonitor_(self._keyMonitor)
		self._keyMonitor = None

	def _keyDown(self, event):
		keyCombination = event.charactersIgnoringModifiers().lower(), event.modifierFlags()
		if keyCombination in ALL_SHORTCUTS and self.w.kerningSequenceView.isFirstResponder():
			self._kernWindowKeyDown(keyCombination)
			return
		self._stopOverridingRFkeys()
		AppKit.NSApp().sendEvent_(event)
		self._startOverridingRFkeys()

	def _selectPairInListCallback(self, sender):
		selection = sender.getSelection()
		if selection != []:
			self._currentPairIndex = selection[0]
			self.currentPair = self._pairlist[self._currentPairIndex]
			self._processGlyphSequence()

	def _incrementKerningValueForCurrentPair(self, value):
		self._addCurrentPairToPairList()
		if not self.RTL:
			value *= -1
		if self.currentPair in self._font.kerning:
			self._font.kerning[self.currentPair] += value
		else:
			self._font.kerning[self.currentPair] = value + int(self.w.kernValueInput.get())
		self._redraw()

	def _incrementPointSize(self, value):
		newSize = self._pointSize + value
		if 200 > newSize >= 30:
			self._pointSize = newSize
			self.w.kerningSequenceView.setPointSize(self._pointSize)

	def _incrementLineHeight(self, value):
		newHeight = self._lineHeight + value
		if newHeight >= 0:
			self._lineHeight = newHeight
			self.w.kerningSequenceView.setLineHeight(self._lineHeight)

	def _addCurrentPairToPairList(self):
		if self.currentPair not in self._pairlist:
			self._pairlist.addPair(self.currentPair, self._currentPairIndex + 1)
			self._updatePairListView(self._currentPairIndex + 1)

	def _incrementPairIndex(self, value):
		self._currentPairIndex += value
		if self._currentPairIndex < 0:
			self._currentPairIndex = self._pairNumbers - 1
		elif self._currentPairIndex == self._pairNumbers:
			self._pairlist.sortBySide()
			self.w.pairListView.set(self._getPairListDict())
			self._currentPairIndex = 0
		self.currentPair = self._pairlist[self._currentPairIndex]
		self.w.pairListView.setSelection([self._currentPairIndex])

	def _removeSelectedPairs(self, answer):
		# if answer = 1, kerning for those pairs also will be removed from the font
		selectedPairsIndices = self.w.pairListView.getSelection()
		selectedPairs = [self._pairlist[i] for i in selectedPairsIndices]
		for i in reversed(selectedPairsIndices):
			self._pairlist.removePairAtIndex(i)
		if answer == 1:
			for pair in selectedPairs:
				if pair in self._font.kerning:
					del self._font.kerning[pair]
		self._updatePairListView(selectedPairsIndices[0])

	def _incrementGroupComboBox(self, value, itemsList, comboBoxObj, setterMethod):
		if value == 1:
			prevItem = comboBoxObj.get()
			prevIndex = itemsList.index(prevItem)
			index = prevIndex + 1
		elif value == 0:
			index = 0
		try:
			item = itemsList[index]
		except IndexError:
			item = itemsList[0]
		comboBoxObj.set(item)
		setterMethod(item)

	def _incrementLeftComboBox(self, value):
		self._incrementGroupComboBox(value, self._leftComboList, self.w.leftEntryComboBox, self._setLeftEntry)

	def _incrementRightComboBox(self, value):
		self._incrementGroupComboBox(value, self._rightComboList, self.w.rightEntryComboBox, self._setRightEntry)

	keyToFunction = {
					INCREASE_LINEHEIGHT_KEY:	("_incrementLineHeight",	(100,	)),
					DECREASE_LINEHEIGHT_KEY:	("_incrementLineHeight",	(-100,	)),
					PREV_PAIR_KEY:				("_incrementPairIndex",		(-1,	)),
					NEXT_PAIR_KEY:				("_incrementPairIndex",		(1,		)),
					ZOOM_IN_KEY:				("_incrementPointSize",		(10,	)),
					ZOOM_OUT_KEY:				("_incrementPointSize",		(-10,	)),
					ClEAR_KERNING_KEY:			("_removeSelectedPairs",	(1,		)),
					LEFT_GLYPH_IN_GROUP_KEY:	("_incrementLeftComboBox",	(1,		)),
					RIGHT_GLYPH_IN_GROUP_KEY:	("_incrementRightComboBox",	(1,		)),
					LEFT_GLYPH_TO_GROUP_KEY:	("_incrementLeftComboBox",	(0,		)),
					RIGHT_GLYPH_TO_GROUP_KEY:	("_incrementRightComboBox",	(0,		)),
					}

	def _kernWindowKeyDown(self, keyCombination):
		key, modifier = keyCombination
		try:
			funcName, args = self.keyToFunction[keyCombination]
			func = getattr(self, funcName)
			func(*args)
			return
		except KeyError:
			kerningIncrement = self.MODIFIER_TO_INCREMENT.get(modifier, 10)
			if key == DECREASE_KERN_KEY[0]:
				self._incrementKerningValueForCurrentPair(kerningIncrement)
			elif key == INCREASE_KERN_KEY[0]:
				self._incrementKerningValueForCurrentPair(-kerningIncrement)
			elif keyCombination in NUMBER_KEYS:
				value = int(key) - 1
				fonts = sorted(self._allFonts.values(), key=lambda f: f.info.openTypeOS2WeightClass)
				try:
					self.setFont(fonts[value])
					self._processGlyphSequence()
				except IndexError:
					return
		self.w.kernValueInput.set(self._font.kerning.get(self.currentPair, 0))

	def _processGlyphSequence(self):
		reverseIndex = 1
		if self.RTL:
			reverseIndex = -1
		if self.currentPair is not None:
			self._kerningViewGlyphSequence = []
			self._kerningViewGlyphSequence.extend(self._glyphNamesToGlyphs(self._referenceGlyphNames[::reverseIndex]))
			self._kerningViewGlyphSequence.append(self._newLineGlyph)
			self._kerningViewGlyphSequence.extend(self._glyphNamesToGlyphs(self._beforeContextGlyphNames[::reverseIndex]))
			self._kerningViewGlyphSequence.extend([self._font[g] for g in self._currentFlatPair])
			self._kerningViewGlyphSequence.extend(self._glyphNamesToGlyphs(self._afterContextGlyphNames[::reverseIndex]))
			self._redraw()

	def _redraw(self):
		self.w.kerningSequenceView.set(self._kerningViewGlyphSequence)

	def _updateWindowEntries(self):
		# To switch entries on rtl/ltr and set kerning value entry on exceptions
		defKernValue = self.w.kernValueInput.get()
		if not defKernValue:
			defKernValue = 0
		else:
			defKernValue = int(defKernValue)
		self.w.kernValueInput.set(self._font.kerning.get(self.currentPair, defKernValue))
		first, second = self.currentPair
		firstGroup = glyphToGroupWithSide(first, self._glyphToGroupMap, 0)
		secondGroup = glyphToGroupWithSide(second, self._glyphToGroupMap, 1)
		if firstGroup is not None:
			firstItem = firstGroup
		else:
			firstItem = first
		if secondGroup is not None:
			secondItem = secondGroup
		else:
			secondItem = second
		self._leftComboList, self._rightComboList = [firstItem], [secondItem]
		self._leftComboList.extend(self._font.groups.get(firstItem, []))
		self._rightComboList.extend(self._font.groups.get(secondItem, []))
		if self.RTL:
			first, second = second, first
			self._leftComboList, self._rightComboList = self._rightComboList, self._leftComboList
			self.w.leftContextInput.set(self._afterContextText)
			self.w.rightContextInput.set(self._beforeContextText)
		else:
			self.w.leftContextInput.set(self._beforeContextText)
			self.w.rightContextInput.set(self._afterContextText)
		if self.w.leftEntryComboBox.getItems() != self._leftComboList:
			self.w.leftEntryComboBox.setItems(self._leftComboList)
		if self.w.rightEntryComboBox.getItems() != self._rightComboList:
			self.w.rightEntryComboBox.setItems(self._rightComboList)
		if self.w.leftEntryComboBox.get() != first:
			self.w.leftEntryComboBox._nsObject.selectItemWithObjectValue_(first)
		if self.w.rightEntryComboBox.get() != second:
			self.w.rightEntryComboBox._nsObject.selectItemWithObjectValue_(second)

	def _lineViewSelectionCallback(self, sender):
		index = sender.getSelection()
		if index is not None:
			glyph = self._kerningViewGlyphSequence[index]
			return glyph

	def setPairList(self, pairlist):
		"""
		Set the pair list to perform kerning on. You can baiscally load the
		font kerning by `setPairList(font.kerning.keys())`

		- pairlist: Should be a list of pairs in the same format as the
		kerning pairs in the font.
		"""
		if pairlist.__class__.__name__ == "PairList":
			self._addPairList(pairlist)
		else:
			self.newPairList()
			self._pairlist.fromList(pairlist)
		self._updatePairListView()
		self._updatePairlistNameComboBox()

	def newPairList(self):
		pairlist = PairList(self._font)
		self._addPairList(pairlist)

	def _addPairList(self, pairlist):
		self._pairlist = pairlist
		self.referenceText = pairlist.referenceText
		self.beforeContext = pairlist.beforeContext
		self.afterContext = pairlist.afterContext
		self._currentPairIndex = 0
		self._pairListsInstructionsDict[self._pairlist.name] = self._pairlist.getInstructions()
		self._pairListDict[self._pairlist.name] = self._pairlist

	def _updatePairListView(self, startIndex=None):
		if startIndex is None:
			startIndex = self._currentPairIndex
		self._pairNumbers = len(self._pairlist)
		self.w.pairListView.set(self._getPairListDict())
		self.w.pairListView.setSelection([startIndex])
		self._processGlyphSequence()

	def _getPairListDict(self):
		return map(self._pairToColumnDict, self._pairlist)

	def _pairToColumnDict(self, pair):
		result = {}
		for i, entry in enumerate(pair):
			column_name = PAIR_COLUMNS[i]
			result[column_name] = self._beautifyEntry(entry)
		return result

	def _beautifyEntry(self, entry):
		rawName = getRawGroupName(entry)
		if rawName is not None:
			return "@ "+rawName
		return entry

def SimpleKerningWindow(font=None):
	if font is None:
		font = CurrentFont()
		if font is None:
			warnings.warn("No font is open!")
			return
	OpenWindow(SKMainWindow, font)
	return getWindow(KERN_WINDOW_KEY)

if __name__ == '__main__':
	from simpleKerning import constants
	from simpleKerning import kernWindow
	from simpleKerning import pairListBuilder
	from simpleKerning.startup import *
	import simpleKerning
	from importlib import reload
	reload(constants)
	reload(simpleKerning)
	reload(kernWindow)
	reload(pairListBuilder)
	from pairListBuilder import *

	f = CurrentFont()
	k = SimpleKerningWindow(f)
	k.setPairList(sorted(f.kerning.keys()))
