import re

MV_OPTIONS = {'displayMode': 'Multi Line',
'Show Kerning': True,
'Multi Line': True,
'xHeight Cut': False,
'Water Fall': False,
'Single Line': True,
'Inverse': False,
'Show Metrics': False,
'Left to Right': False,
'Right to Left': True,
'Center': True,
'Upside Down': False,
'Stroke': False,
'Fill': True,
'Beam': False,
'Guides': False,
'Blues': False,
'Family Blues': False,
'Show Control glyphs': False,
'Show Space Matrix': False,
'Show Template Glyphs': False,
'showLayers': []}

EXTENSION_KEY = 'design.bahman.simpleKerning.'
EXTENSION_PAIRLIST_KEY = EXTENSION_KEY + "pairLists"
KERN_WINDOW_KEY = EXTENSION_KEY + 'kerningWindow'
GROUP_WINDOW_KEY = EXTENSION_KEY + 'groupingWindow'
RTL_KEY = EXTENSION_KEY + 'rtl'
RE_GROUP_TAG = re.compile(r'public.kern\d\.')
GROUP_SIDE_TAG = ("public.kern1.", "public.kern2.")
FIRST_COLUMN = '1st'
SECOND_COLUMN = '2nd'
PAIR_COLUMNS = (FIRST_COLUMN, SECOND_COLUMN)
NO_MODIFIER_CODE = 256
SHFIT_CODE = 131330
SHIFT_CMD_CODE = 1179914
ALT_CODE = 524576
NEXT_PAIR_KEY = ("s", NO_MODIFIER_CODE)
PREV_PAIR_KEY = ("w", NO_MODIFIER_CODE)
INCREASE_KERN_KEY = ("d", NO_MODIFIER_CODE)
DECREASE_KERN_KEY = ("a", NO_MODIFIER_CODE)
ZOOM_IN_KEY = ("z", NO_MODIFIER_CODE)
ZOOM_OUT_KEY = ("x", NO_MODIFIER_CODE)
INCREASE_LINEHEIGHT_KEY = ("s", SHFIT_CODE)
DECREASE_LINEHEIGHT_KEY = ("w", SHFIT_CODE)
ClEAR_KERNING_KEY = ("c", SHFIT_CODE)
LEFT_GLYPH_IN_GROUP_KEY = ("q", NO_MODIFIER_CODE)
RIGHT_GLYPH_IN_GROUP_KEY = ("e", NO_MODIFIER_CODE)
LEFT_GLYPH_TO_GROUP_KEY = ("q", SHFIT_CODE)
RIGHT_GLYPH_TO_GROUP_KEY = ("e", SHFIT_CODE)
ALL_SHORTCUTS = set((
				NEXT_PAIR_KEY,
				PREV_PAIR_KEY,
				INCREASE_KERN_KEY,
				DECREASE_KERN_KEY,
				ZOOM_IN_KEY,
				ZOOM_OUT_KEY,
				INCREASE_LINEHEIGHT_KEY,
				DECREASE_LINEHEIGHT_KEY,
				ClEAR_KERNING_KEY,
				LEFT_GLYPH_IN_GROUP_KEY,
				RIGHT_GLYPH_IN_GROUP_KEY,
				LEFT_GLYPH_TO_GROUP_KEY,
				RIGHT_GLYPH_TO_GROUP_KEY,
				))
MODFIERS = SHFIT_CODE, SHIFT_CMD_CODE, ALT_CODE
for modifier in MODFIERS:
	ALL_SHORTCUTS.update(((INCREASE_KERN_KEY[0], modifier), (DECREASE_KERN_KEY[0], modifier)))
NUMBER_KEYS = [(str(i), NO_MODIFIER_CODE) for i in range(10)]
ALL_SHORTCUTS.update(NUMBER_KEYS)
