# What
A simple kerning and grouping solution in RoboFont.

# Why
- Support RTL/LTR kerning
- Simple and clean UI
- Pair list python module
- Save pair lists under a name
- Free and open source

# Grouping Window
![](img/simple-grouping-window.png)
1. Open the group window `Extensions > Simple Kerning > Grouping Window`.
2. Select the glyphs you want to have in a group.
3. Choose the side (left/right) that looks similar in the selected glyphs.
4. Put the name of the group in the window.
5. Observe how they overlap in the glyph window.

# Pair Lists
Creating pair lists requires to run a small code. Don't worry, It's very simple!

## Build pairs from characters
You need two sets of characters to make the kerning pair list. For example
uppercase characters next to lowercase ones, or letters next to punctuation.
You can make these using a small code shown here. Run these samples in the
python scripting window to see the results. First you need to import `PairList`
and `SimpleKerningWindow` object and create a pair list instance:

```py
from simpleKerning.kernWindow import SimpleKerningWindow
from simpleKerning.pairListBuilder import PairList
f = CurrentFont()
myPairList = PairList(f)
```

As you can see the `PairList` object takes the font object as the argument.
Now we can create our pair list from text:

- ### Uppercase with Lowercase

  We can store the `lowercase` and `uppercase` letters inside two variables
  then make the pair list:

```py
lowercase = "abcdefghijklmnopqrstuvwxyz"
uppercase = lowercase.upper()
myPairList.fromText(uppercase, lowercase)
```

- ### Lowercase with closing punctuation

  Here we use the same `lowercase` variable and define a new one for punctuation:

```py
closing = "}›»’”)].:"
myPairList.fromText(lowercase, closing)
```

- ### Opening punctuation with uppercase

  Here we use the same `uppercase` variable and define a new one for punctuation:

```py
opening = "{‹«‘“([.:"
myPairList.fromText(opening, uppercase)
```

- ### Uppercase with small caps
  We can use suffixes in the glyph names to create pair lists:
```py
myPairList.fromText(uppercase, lowercase, secondSuffixes=["smcp"])
```
  In the above example name of the small caps glyphs are driven from lowercase glyphs as in `a.scmp`.

- ### Glyphs with `fina` suffix next to `init` and `isol`
```py
vared = 'دراو'
connecting = 'بپجچحخسطعقکلمںنهیى'
myPairList.fromText(vared, connecting, firstSuffixes=["fina"], secondSuffixes=["init", "isol"])
```

Every time we run the `PairList.fromText` method, the pair list gets
extended. So we can run this method as much as we need to create a larger
pair list.

## load pairs into the kerning window
After creating our pair list, we should load it into the kerning window:
```py
k = SimpleKerningWindow()
k.setPairList(myPairList)
```

## Load pairs from kerning inside the font
Every time you open the kerning window, the following code gets executed:

```py
from simpleKerning.kernWindow import SimpleKerningWindow

f = CurrentFont()
k = SimpleKerningWindow()
k.setFont(f)
k.setPairList(sorted(f.kerning.keys()))
```

As you can see the pair list items is exactly same as the items in the
`font.kerning` object. Alternately you can make your own kerning items and
load it using the above method; but if you name this pair list in the UI, the
instructions will not be saved under that name. If you want the pair list
instructions to be saved under the name, the pair list has to be an instance
of `PairList`.

# Kerning Window

![](img/simple-kerning-window.png)

The reference text is for comparing the kerning of already kerned pairs to the
current pair. If you create a pair list using the above method and write a
name for it in the pair list name. The following information will be loaded
every time you select a pair list from the pair list names combo box:
- Instructions used to make the pair list
- Reference text
- Before/After context

The pair list instructions will be loaded on other fonts no matter what groups
or glyph names they have (except for the suffixes in glyph names), because the
instructions is based on unicodes and suffixes in glyph names.

All the text boxes in the UI are open for entering a value. You can also type
inside any of the following fields and if the input is valid it will be applied
otherwise it will be ignored:

- Input for Kerning Value
- Input for Left/Right group or glyph in the current pair


## Hot keys

You need to click on the kerning view to make the hot keys work. The hot keys
are designed according to PC gaming conventions. This makes it possible to
have your right hand free on the mouse and your left wrist fixed while kerning.

|   Action   |  Hot key    |
| ---- | ---- |
|   Next pair   |   s   |
|   Previous pair   |   w   |
|   Increase Kerning 10 units   |   d  |
|   Decrease Kerning 10 units   |  a  |
|   Increase Kerning 50 units   |  ⇧ + d  |
|   Decrease Kerning 50 units   |  ⇧ + a  |
|   Increase Kerning 100 units   |  ⇧ + ⌘ + d  |
|   Decrease Kerning 100 units   |  ⇧ + ⌘ + a  |
|   Increase Kerning 1 unit   |  ⌥ + d  |
|   Decrease Kerning 1 unit   |  ⌥ + a  |
|   Increase line height   |  ⇧ + s   |
|   Decrease line height   |  ⇧ + w   |
|   Clear kerning for the selected pairs   |  ⇧ + c   |
|   Next glyph in left group   |   q   |
|   Next glyph in right group   |   e   |
|   Convert left glyph to group   |   ⇧ + q   |
|   Convert right glyph to group   |   ⇧ + e   |
|   Next font   |   1, 2, 3, ...   |
|   Increase point size   |   z   |
|   Decrease point size   |   x   |

**Note:** If you reach and pass end of the pair list using the `s` hot key,
the pair list will be sorted based on the 1st or 2nd item of the pairs,
depending on which was the last sort.

## Adding a kerning exception
1. Cycle through the glyphs for the current pair using either `q` or `e` hot
keys. 
2. When you see a glyph that needs an exception, use `a` or `d` to change
its kerning. This will add that pair as an exception. 

## Converting a glyph to a group in the current pair
If you want to go back to group kerning on left or right side, you can use
either `⇧ + q` or `⇧ + e` to convert that side to a group. If the glyph on
that side is not in a kerning group, nothing will happen.

## Removing kerning
You can select the pairs on the pair list items and then delete the kerning
value in the kerning value input field. If you don't select any pairs, by
default the current pair will be removed.

Another method is while you're kerning you can hit `⇧ + c` to remove the
current pair or the selected pairs. Make sure you click on the current kerning
view otherwise the hot key doesn't work.

Before removing a pair from a font, a window will open that will ask you how
to proceed.

## Version History
* 1.0 - Public Release

## Thanks to
* Erik Van Blockland for inspirations. Erik wrote an extension for right to left kerning which I have been using until RoboFont 3 came out. Some UI in this extension is also inspired by that.
* Tal Leming for the libraries this tool and its UI is made of.
* Frederik Berlaen for showing me how to hack RoboFont!
* Frank Grießhammer for writing so many open source codes for kerning which I had used in some parts of this extension.

## Author
Copyright (c) 2020 Bahman Eslami

## License
MIT
