from simpleKerning import constants
from simpleKerning import pairListBuilder
from simpleKerning.startup import *
import simpleKerning
from importlib import reload
reload(constants)
reload(simpleKerning)
reload(pairListBuilder)
from simpleKerning.pairListBuilder import *
from simpleKerning.kernWindow import SimpleKerningWindow

ARABIC_SUFFIXES = ('init', 'medi', 'fina', 'isol')  # if you want to change this, the order should retain same
OPENING_PUNCTUATION = '}›»’”)]'  # first entry in pair, second entry is isol or init
CLOSING_PUNCTUATION = '{‹«‘“(['  # second enry in pair, fist entry is isol or fina
NEUTRAL_CLOSING_PUNCTUATION = '.:'  # Both for Latin or Arabic
MATH_SYMBOLS = '−+±×=÷<>≈≠≤≥~#'
ARABIC_CLOSING_PUNCTUATION = '،؛؟'  # second enry in pair, fist entry is isol or fina
ARABIC_MATH_SYMBOLS = '؍٫٪%٬'  # other entry should be a hindu arabic numeral
ARABIC_VARED = 'آأٲإٱادذرزژوؤ'  # letter with only two representaiotn shapes, but not kerned as VARED
ARABIC_CONNECTING = 'ٮبپتثجچحخسشصضطظعغڡفڤقکگكلمںنهﻩةیيئى'
ARABIC_ALL_LETTERS = 'ء' + ARABIC_VARED + ARABIC_CONNECTING
ARABIC_NUMERALS = '٠١٢٣٤٥٦٧٨٩'
PERSIAN_NUMERALS = '۰۱۲۳۴۵۶۷۸۹'

f = CurrentFont()

# letters
arLetters = PairList(f)
arLetters.fromText(ARABIC_VARED, ARABIC_ALL_LETTERS)
arLetters.fromText(ARABIC_VARED, ARABIC_ALL_LETTERS, secondSuffixes=["init", "isol"])
arLetters.fromText(ARABIC_VARED, ARABIC_ALL_LETTERS, firstSuffixes=["isol"], secondSuffixes=["init", "isol"])
arLetters.fromText(ARABIC_VARED, ARABIC_ALL_LETTERS, firstSuffixes=["fina"], secondSuffixes=["init", "isol"])
arLetters.fromText(ARABIC_VARED, ARABIC_ALL_LETTERS, firstSuffixes=["fina"])
k = SimpleKerningWindow()
k.setFont(f)
k.setPairList(arLetters)
